package Helpers;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import Helpers.TestUtil;
public class browserFactory {

	static WebDriver driver;
	public static WebDriver startbrowser(String BrowserName) throws IOException
	{
		
		if (BrowserName.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			
		    Properties prop = TestUtil.readPropertiesFile("TestData\\data.properties");
			driver.get(prop.getProperty("URL"));
			driver.manage().window().maximize();
		}
		return driver;
		
	}
}
