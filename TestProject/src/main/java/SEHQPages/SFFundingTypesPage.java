package SEHQPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SFFundingTypesPage {

	WebDriver driver;
	//public WebDriver driver;
		public SFFundingTypesPage(WebDriver driver)
		{
			//this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(how=How.XPATH, using="//span[@class='slds-truncate'][contains(text(),'Funding Types')]")
		WebElement link_FundingTypes;
		
		@FindBy(how=How.XPATH, using="//a[@title='New']")
		WebElement btn_New;
				
		@FindBy(how=How.XPATH, using="//label[contains(@class,'label inputLabel uiLabel-left form-element__label uiLabel')]//span[contains(text(),'Name')]//following::input[1]")
		WebElement txtBox_Name;
		
		
		@FindBy(how=How.XPATH, using="//button[@title='Save']")
		WebElement btn_Save;
		
//		@FindBy(how=How.XPATH, using="//slot[@name='primaryField']//slot//lightning-formatted-text[contains(text(),'\"+\"testNaveen1\"+\"')]")
//		WebElement txt_Expected;
		
		public void createFundingTypes(WebDriver driver, String strName) throws InterruptedException
		{ 
			System.out.println("in funding types function");
			Actions actions1 = new Actions(driver);
		    actions1.moveToElement(link_FundingTypes).click().build().perform();
		    
			//link_FundingTypes.click();
			
			Thread.sleep(5000);
			//btn_New.click();
			//Actions actions1 = new Actions(driver);
		    actions1.moveToElement(btn_New).click().build().perform();
		      
		    Thread.sleep(3000);
			txtBox_Name.sendKeys(strName);
			
			btn_Save.click();
		}
}
