package SEHQPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SFLoginPage {

	//public WebDriver driver;
	public SFLoginPage(WebDriver driver)
	{
		//this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//input[@id='username']")
	WebElement txt_UserName;
	
	@FindBy(how=How.XPATH, using="//input[@id='password']")
	WebElement txt_Pwd;
	
	@FindBy(how=How.XPATH, using="//input[@id='Login']")
	WebElement btn_Login;
	
	public void login(String uname, String pwd)
	{
		System.out.println("in login function");
		System.out.println(uname);
		System.out.println(pwd);
		
		txt_UserName.sendKeys(uname);
		txt_Pwd.sendKeys(pwd);
		btn_Login.click();
		
	}
}
