package stepDefinations;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Helpers.TestUtil;
import Helpers.browserFactory;
import SEHQPages.SFFundingTypesPage;
import SEHQPages.SFLoginPage;
import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class stepDefination {
		browserFactory browserObj;
		WebDriver driver;
		SFLoginPage objSFLoginPage;
		SFFundingTypesPage objSFFundingTypesPage;
		//TestUtil ObjUtil = new TestUtil();
		
    
	    @Given("^Parent is on landing page$")
	    public void parent_is_on_landing_page() throws Throwable {
	        // Write code here that turns the phrase above into concrete actions
	      System.out.println("in given function");
	      driver = browserFactory.startbrowser("chrome");
	      objSFLoginPage = new SFLoginPage(driver);
	      driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	      
	      Properties prop = TestUtil.readPropertiesFile("TestData\\data.properties");
//	      System.out.println(prop.getProperty("username"));
//	      System.out.println(prop.getProperty("password"));
	      
//	      driver.findElement(By.xpath("//input[@id='username']")).sendKeys(prop.getProperty("username"));
//	      driver.findElement(By.xpath("//input[@id='password']")).sendKeys(prop.getProperty("password"));
//	    
//	      driver.findElement(By.xpath("//input[@id='Login']")).click();
	      Thread.sleep(15000);
	      
	      objSFLoginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	      Thread.sleep(25000);
	      
	     // driver.findElement(By.xpath("//span[@class='slds-truncate'][contains(text(),'Funding Types')]")).click();
//	      WebDriverWait wait = new WebDriverWait(driver, 15);
//	      System.out.println("b4 wait1");
//	      wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='slds-truncate'][contains(text(),'Funding Types')]"))).click();
//	      System.out.println("after wait1");
//	      driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
//	      LOGGER.info("element is " +element);
//	      LOGGER.info(String.format("Waiting for WebElement '%s' to be displayed", element.toString().replaceAll(".*-> ", "").replace("]", "")));
	    
//	      WebElement element = driver.findElement(By.xpath("//span[@class='slds-truncate'][contains(text(),'Funding Types')]"));
//	      Actions actions = new Actions(driver);
//	      actions.moveToElement(element).click().build().perform();
//	      System.out.println("after clicking");
	      
	      Thread.sleep(10000);
	      
	  
	      //actions.perform();
	      
	    }

	    @When("^Parent login into survey application with username and password$")
	    public void parent_login_into_survey_application_with_username_and_password() throws Throwable {
	        // Write code here that turns the phrase above into concrete actions
	    	System.out.println("in when function");
	    	 // click on New button	
//		      WebElement element1 =  driver.findElement(By.xpath("//a[@title='New']"));
//		      Actions actions1 = new Actions(driver);
//		      actions1.moveToElement(element1).click().build().perform();
//		      System.out.println("after clicking");
//		      
//		      Thread.sleep(3000);
		      
		    // click on New button	      
		      //driver.findElement(By.xpath("//div[contains(text(),'New')]")).click();
		      
		   //   driver.findElement(By.xpath("//label[contains(@class,'label inputLabel uiLabel-left form-element__label uiLabel')]//span[contains(text(),'Name')]//following::input[1]")).sendKeys("testNaveen2");
		      
		      
//		      Thread.sleep(3000);
		      //Click on Save button
		     // driver.findElement(By.xpath("//button[@title='Save']")).click();
		      
	    }

	    @Then("^Parent should be able to create funding type successfully$")
	    public void parent_should_be_able_to_login_successfully() throws Throwable {
	        // Write code here that turns the phrase above into concrete actions
	    	System.out.println("in Then function");
	    	objSFFundingTypesPage = new SFFundingTypesPage(driver);
	    	Thread.sleep(3000);
	    	//driver.findElement(By.xpath("//span[@class='slds-truncate'][contains(text(),'Funding Types')]")).click();
	    	objSFFundingTypesPage.createFundingTypes(driver,"testNaveen2");
	    	
	    	//String text1= "testNaveen";
	    	String xpath1 = "//slot[@name='primaryField']//slot//lightning-formatted-text[contains(text(),'')]";
	    	String strActualtext = driver.findElement(By.xpath(xpath1)).getText();
	    	System.out.println("actual text is -"+strActualtext);
	    	
	    	
	    	
	    	if (strActualtext=="testNaveen2")
	    	{
	    		System.out.println("text matched and passed");
	    	}
	    	else 
	    	{
	    		TestUtil.takeSnapShot(driver, "Screenshot\\"+Scenario.class+".png");
	    	}
	    	
	    }
}
