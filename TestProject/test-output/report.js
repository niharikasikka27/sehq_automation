$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("SEHQLogin.feature");
formatter.feature({
  "line": 1,
  "name": "SEHQ Survey login",
  "description": "",
  "id": "sehq-survey-login",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "SEHQ Basic survey login",
  "description": "",
  "id": "sehq-survey-login;sehq-basic-survey-login",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Parent is on landing page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Parent login into survey application with username and password",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Parent should be able to login successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefination.parent_is_on_landing_page()"
});
formatter.result({
  "duration": 9693774300,
  "status": "passed"
});
formatter.match({
  "location": "stepDefination.parent_login_into_survey_application_with_username_and_password()"
});
formatter.result({
  "duration": 88100,
  "status": "passed"
});
formatter.match({
  "location": "stepDefination.parent_should_be_able_to_login_successfully()"
});
formatter.result({
  "duration": 104900,
  "status": "passed"
});
});